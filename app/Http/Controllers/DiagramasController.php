<?php

namespace App\Http\Controllers;

use App\Bot;
use App\User;
use App\Pregunta;
use App\PreguntasUsuario;
use App\Categoria;
use App\Diagrama;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class DiagramasController extends Controller
{
   public function index() 
   {
       if (Auth::check())
       {
            $menu = Diagrama::whereIn('pregunta_id', Pregunta::select('id')->whereIn('categoria_id', Categoria::select('id')->where('descripcion', 'menu')->where('bot_id', Session::get('bot_id'))))->orderBy('letra', 'asc')->get();
            $diagramas = Diagrama::where('bot_id', Session::get('bot_id'))->get();
            return view('bot.diagramas.index', compact('diagramas', 'menu'));
        } else {
            return redirect()->route('voyager.login');
        }
   }
   public function add_hijo(Request $request)
   {
        $r = $request->all();
        $padre_id = $r['padre_id'];
        $category_nombre = $r['categoria'];
        $palabras = explode(" ", $category_nombre);
        $categoria = '';
        foreach ($palabras as $key => $value) {
            $categoria = $categoria . '_' . strtolower($value);
        }
        $categoria = substr($categoria, 1);
        $pregunta_modelo = $r['pregunta_modelo'];
        $respuesta = $r['respuesta'];
        $descripcion = $r['descripcion'];

        $cat = new Categoria;
        $cat->descripcion = $categoria;
        $cat->bot_id = Session::get('bot_id');
        if ($cat->save()) {
            $preg = new Pregunta;
            $preg->pregunta_modelo = $pregunta_modelo;
            $preg->categoria_id = $cat->id;
            $preg->descripcion = $respuesta;
            if ($preg->save()) {
                $last_letter = Diagrama::where('pregunta_id', $padre_id)->orderBy('letra', 'desc')->first();
                if ($last_letter) {
                    $new_letter = strtoupper($last_letter->letra);
                    $new_letter++;
                } else {
                    $new_letter = 'A';
                }
                
                $diag = new Diagrama;
                $diag->descripcion = $descripcion;
                $diag->respuesta_id = $preg->id;
                $diag->pregunta_id = $padre_id;
                $diag->letra = $new_letter;
                $diag->bot_id = Session::get('bot_id');
                if ($diag->save()) {
                    return response()->json('Listo!, la pagina se refrescará en unos segundos', 200);
                } else {
                    $cat->delete();
                    $preg->delete();
                    return response()->json('No se pudo crear el diagrama.', 500);
                }

            } else {
                $cat->delete();
                return response()->json('No se pudo crear la respuesta', 500);
            }
        } else {
            return response()->json('Ocurrio un error al guardar la categoria', 500);
        }
   }
    public function add_hijo_existente(Request $request)
    {
        $r = $request->all();
        $padre_id = $r['padre_id'];
        $pregunta_id = $r['pregunta'];
        $descripcion = $r['descripcion'];
        $last_letter = Diagrama::where('pregunta_id', $padre_id)->orderBy('letra', 'desc')->first();
        if ($last_letter) {
            $new_letter = strtoupper($last_letter->letra);
            $new_letter++;
        } else {
            $new_letter = 'A';
        }
        $diag = new Diagrama;
        $diag->descripcion = $descripcion;
        $diag->respuesta_id = $pregunta_id;
        $diag->pregunta_id = $padre_id;
        $diag->letra = $new_letter;
        $diag->bot_id = Session::get('bot_id');
        if ($diag->save()) {
            return response()->json('Listo!, la pagina se refrescará en unos segundos', 200);
        } else {
            return response()->json('No se pudo crear el diagrama.', 500);
        }
        
    }
   public function add_nueva_opcion_menu_ppal(Request $request)
   {
        $r = $request->all();
        $category_nombre = $r['categoria'];
        $palabras = explode(" ", $category_nombre);
        $categoria = '';
        foreach ($palabras as $key => $value) {
            $categoria = $categoria . '_' . strtolower($value);
        }
        $categoria = substr($categoria, 1);
        $pregunta_modelo = $r['pregunta_modelo'];
        $respuesta = $r['respuesta'];
        $descripcion = $r['descripcion'];

        $padre_search = Pregunta::whereIn('categoria_id', Categoria::select('id')->where('descripcion', 'menu'))->first();
        if ($padre_search) {
            $padre_id = $padre_search->id;
        } else {
            $categ = new Categoria;
            $categ->descripcion = 'menu';
            $categ->bot_id = Session::get('bot_id');
            if ($categ->save()) {
                $pregun = new Pregunta;
                $pregun->categoria_id = $categ->id;
                $pregun->pregunta_modelo = 'Menú Principal';
                $pregun->descripcion = 'Por favor indícame, <i class="em em-thinking_face"></i> ¿Sobre qué quieres información?';
                if ($pregun->save()) {
                    $padre_id = $pregun->id;
                    $find = Categoria::where('bot_id', Session::get('bot_id'))->where('descripcion', 'bienvenida')->first();
                    if ($find) {
                    } else {
                        $bienvenida = new Categoria;
                        $bienvenida->descripcion = 'bienvenida';
                        $bienvenida->bot_id = Session::get('bot_id');
                        if ($bienvenida->save()) {
                            $bienv = new Pregunta;
                            $bienv->categoria_id = $bienvenida->id;
                            $bienv->pregunta_modelo = '¡Hola!';
                            $bienv->descripcion = '<i>Hola! Soy una asistente automatizada diseñada por la Comisión de Inteligencia Artificial de la Corte Suprema de Justicia de Tucumán.</i><br/><br/>  <i class="em em-robot_face"></i> Puedo responder preguntas sobre ' . Session::get('bot_descripcion') . ' <br/><br/> <i class="em em-exclamation"></i><i>No almacenamos ni transferimos información personal que pueda surgir de nuestra conversación. Tampoco utilizamos cookies propias o de terceros.</i><br/><br/> Puedes comunicarte con la Atención al Ciudadano: 0800-444-0761 (Lunes a Viernes 7 a 13 Hs.)<br/><br/> <i class="em em-point_right"></i> <a href="javascript:send_void();">▶ Click Aquí ◀</a> para visualizar el menú.<br/>Si deseas volver al menú principal sólo debes escribirme MENU.';
                            $bienv->picture = 'public/storage/preguntas/June2020/C5pj800rWm4eIwbzG8Ym.jpg';
                            if ($bienv->save())
                            {
                                $preg_usu = new PreguntasUsuario;
                                $preg_usu->pregunta = 'mmenu';
                                $preg_usu->pregunta_id = $pregun->id;
                                $preg_usu->user_id = Auth::user()->id;
                                if ($preg_usu->save())
                                {
                                    $no_tengo_esa_info = new Categoria;
                                    $no_tengo_esa_info->descripcion = 'no_tengo_esa_informacion';
                                    $no_tengo_esa_info->bot_id = Session::get('bot_id');
                                    if ($no_tengo_esa_info->save()) {
                                        $resp_no_tengo_esa_info = new Pregunta;
                                        $resp_no_tengo_esa_info->pregunta_modelo = '<i class="em em-confused"></i> Oops!. Parece que no tengo esa información todavía. ¿Podrías preguntarme otra cosa?.';
                                        $resp_no_tengo_esa_info->descripcion = 'Es posible que no esté preparado para responder ese tipo de preguntas.';
                                        $resp_no_tengo_esa_info->categoria_id = $no_tengo_esa_info->id;
                                        $resp_no_tengo_esa_info->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $cat = new Categoria;
        $cat->descripcion = $categoria;
        $cat->bot_id = Session::get('bot_id');
        if ($cat->save()) {
            $preg = new Pregunta;
            $preg->pregunta_modelo = $pregunta_modelo;
            $preg->categoria_id = $cat->id;
            $preg->descripcion = $respuesta;
            if ($preg->save()) {
                $last_letter = Diagrama::where('pregunta_id', $padre_id)->orderBy('letra', 'desc')->first();
                if ($last_letter) {
                    $new_letter = strtoupper($last_letter->letra);
                    $new_letter++;
                } else {
                    $new_letter = 'A';
                }

                $diag = new Diagrama;
                $diag->descripcion = $descripcion;
                $diag->respuesta_id = $preg->id;
                $diag->pregunta_id = $padre_id;
                $diag->letra = $new_letter;
                $diag->bot_id = Session::get('bot_id');
                if ($diag->save()) {
                    return response()->json('Listo!, la pagina se refrescará en unos segundos', 200);
                } else {
                    $cat->delete();
                    $preg->delete();
                    return response()->json('No se pudo crear el diagrama.', 500);
                }
            } else {
                $cat->delete();
                return response()->json('No se pudo crear la respuesta', 500);
            }
        } else {
            return response()->json('Ocurrio un error al guardar la categoria', 500);
        }
   }
    public function add_nuevo_menu_al_menu_ppal(Request $request)
    {
        $r = $request->all();
        $categoria = $r['categoria'];

        $padre_search = Pregunta::whereIn('categoria_id', Categoria::select('id')->where('descripcion', 'menu')->where('bot_id', Session::get('bot_id')))->first();
        if ($padre_search) {
            $padre_id = $padre_search->id;
        } else {
            $categ = new Categoria;
            $categ->descripcion = 'menu';
            $categ->bot_id = Session::get('bot_id');
            if ($categ->save()) {
                $pregun = new Pregunta;
                $pregun->pregunta_modelo = 'Menú Principal';
                $pregun->descripcion = 'Por favor indícame, <i class="em em-thinking_face"></i> ¿Sobre qué quieres información?';
                if ($pregun->save()) {
                    $padre_id = $pregun->id;
                    $find = Categoria::where('bot_id', Session::get('bot_id'))->where('descripcion', 'bienvenida')->first();
                    if ($find) { 
                    } else {
                        $bienvenida = new Categoria;
                        $bienvenida->descripcion = 'bienvenida';
                        $bienvenida->bot_id = Session::get('bot_id');
                        if ($bienvenida->save()) {
                            $bienv = new Pregunta;
                            $bienv->categoria_id = $bienvenida->id;
                            $bienv->pregunta_modelo = '¡Hola!';
                            $bienv->descripcion = '<i>Hola! Soy una asistente automatizada diseñada por la Comisión de Inteligencia Artificial de la Corte Suprema de Justicia de Tucumán.</i><br/><br/>  <i class="em em-robot_face"></i> Puedo responder preguntas sobre ' . Session::get('bot_descripcion') . ' <br/><br/> <i class="em em-exclamation"></i><i>No almacenamos ni transferimos información personal que pueda surgir de nuestra conversación. Tampoco utilizamos cookies propias o de terceros.</i><br/><br/> Puedes comunicarte con la Atención al Ciudadano: 0800-444-0761 (Lunes a Viernes 7 a 13 Hs.)<br/><br/> <i class="em em-point_right"></i> <a href="javascript:send_void();">▶ Click Aquí ◀</a> para visualizar el menú.<br/>Si deseas volver al menú principal sólo debes escribirme MENU.';
                            $bienv->picture = 'public/storage/preguntas/June2020/C5pj800rWm4eIwbzG8Ym.jpg';
                            if ($bienv->save()) {
                                $preg_usu = new PreguntasUsuario;
                                $preg_usu->pregunta = 'mmenu';
                                $preg_usu->pregunta_id = $pregun->id;
                                $preg_usu->user_id = Auth::user()->id;
                                if ($preg_usu->save()) {
                                    $no_tengo_esa_info = new Categoria;
                                    $no_tengo_esa_info->descripcion = 'no_tengo_esa_informacion';
                                    $no_tengo_esa_info->bot_id = Session::get('bot_id');
                                    if ($no_tengo_esa_info->save())
                                    {
                                        $resp_no_tengo_esa_info = new Pregunta;
                                        $resp_no_tengo_esa_info->pregunta_modelo = '<i class="em em-confused"></i> Oops!. Parece que no tengo esa información todavía. ¿Podrías preguntarme otra cosa?.';
                                        $resp_no_tengo_esa_info->descripcion = 'Es posible que no esté preparado para responder ese tipo de preguntas.';
                                        $resp_no_tengo_esa_info->categoria_id = $no_tengo_esa_info->id;
                                        $resp_no_tengo_esa_info->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $palabras = explode(" ", $categoria);
        $category_nombre = '';
        foreach ($palabras as $key => $value) {
            $category_nombre = $category_nombre.'_'.strtolower($value);
        }
        $category_nombre = substr($category_nombre.'_menu', 1); 

        $cat = new Categoria;
        $cat->descripcion = $category_nombre;
        $cat->bot_id = Session::get('bot_id');
        if ($cat->save()) {
            $preg = new Pregunta;
            $preg->pregunta_modelo = '¿Qué información sobre '. $categoria .' necesitas?';
            $preg->categoria_id = $cat->id;
            $preg->descripcion = '<i class="em em-arrow_down"></i> Por favor selecciona.';
            if ($preg->save()) {
                $last_letter = Diagrama::where('pregunta_id', $padre_id)->orderBy('letra', 'desc')->first();
                if ($last_letter) {
                    $new_letter = strtoupper($last_letter->letra);
                    $new_letter++;
                } else {
                    $new_letter = 'A';
                }

                $diag = new Diagrama;
                $diag->descripcion = $categoria;
                $diag->respuesta_id = $preg->id;
                $diag->pregunta_id = $padre_id;
                $diag->letra = $new_letter;
                $diag->bot_id = Session::get('bot_id');
                if ($diag->save()) {
                    return response()->json('Listo!, la pagina se refrescará en unos segundos', 200);
                } else {
                    $cat->delete();
                    $preg->delete();
                    return response()->json('No se pudo crear el diagrama.', 500);
                }
            } else {
                $cat->delete();
                return response()->json('No se pudo crear la respuesta', 500);
            }
        } else {
            return response()->json('Ocurrio un error al guardar la categoria', 500);
        }
    }
    public function add_nuevo_menu_a_menu(Request $request)
    {
        $r = $request->all();
        $categoria = $r['categoria'];
        $padre_id = $r['padre_id'];

        $palabras = explode(" ", $categoria);
        $category_nombre = '';
        foreach ($palabras as $key => $value) {
            $category_nombre = $category_nombre . '_' . strtolower($value);
        }
        $category_nombre = substr($category_nombre . '_menu', 1);

        $cat = new Categoria;
        $cat->descripcion = $category_nombre;
        $cat->bot_id = Session::get('bot_id');
        if ($cat->save()) {
            $preg = new Pregunta;
            $preg->pregunta_modelo = '¿Qué información sobre ' . $categoria . ' necesitas?';
            $preg->categoria_id = $cat->id;
            $preg->descripcion = '<i class="em em-arrow_down"></i> Por favor selecciona.';
            if ($preg->save()) {
                $last_letter = Diagrama::where('pregunta_id', $padre_id)->orderBy('letra', 'desc')->first();
                if ($last_letter) {
                    $new_letter = strtoupper($last_letter->letra);
                    $new_letter++;
                } else {
                    $new_letter = 'A';
                }

                $diag = new Diagrama;
                $diag->descripcion = $categoria;
                $diag->respuesta_id = $preg->id;
                $diag->pregunta_id = $padre_id;
                $diag->letra = $new_letter;
                $diag->bot_id = Session::get('bot_id');
                if ($diag->save()) {
                    return response()->json('Listo!, la pagina se refrescará en unos segundos', 200);
                } else {
                    $cat->delete();
                    $preg->delete();
                    return response()->json('No se pudo crear el diagrama.', 500);
                }
            } else {
                $cat->delete();
                return response()->json('No se pudo crear la respuesta', 500);
            }
        } else {
            return response()->json('Ocurrio un error al guardar la categoria', 500);
        }
    }
    public function quitar_de_menu(Request $request)
    {
        $r = $request->all();
        $diagrama = Diagrama::where('respuesta_id', $r['respuesta_id'])->where('pregunta_id', $r['pregunta_id'])->first();
        if ($diagrama->delete())
        {
            return response()->json('Listo, se quitó esa opción del menú.', 200);
        } else {
            return response()->json('Hubo un error al tratar de quitar del menú esa opción', 500);
        }
    }
    public function entrenar($pregunta_id, $pregunta)
    {
        $exists = DB::table("preguntas_usuarios")->whereRaw('upper(pregunta) = upper("' . $pregunta . '")')->first();
        if (!$exists) {
            $new = new PreguntasUsuario;
            $new->pregunta_id = $pregunta_id;
            $new->user_id = auth()->user()->id;
            $new->pregunta = $pregunta;
            $new->revisada = 0;
            if ($new->save()) {
                return response()->json('Listo!', 200);
            } else {
            return response()->json('Error!', 500);
            }
        } else {
            return response()->json('La pregunta ya existe', 200);
        }
    }
    public function eliminar($pregunta_id)
    {
        $pregunta = Pregunta::find($pregunta_id);
        if ($pregunta) {
            $categoria = Categoria::find($pregunta->categoria_id);
            if ($categoria)
            {
                $preg_usuarios = PreguntasUsuario::where('pregunta_id', $pregunta_id)->get();
                foreach ($preg_usuarios as $p)
                {
                    $p->delete();
                }
                $diag = Diagrama::where('pregunta_id', $pregunta_id)->orWhere('respuesta_id', $pregunta_id)->get();
                foreach ($diag as $d)
                {
                    $d->delete();
                }
                if ($categoria->delete())
                {
                    $pregunta->delete();
                    return response()->json('Listo!', 200);
                } else {
                    return response()->json('No se pudo eliminar la pregunta', 599);
                }
            } else {
                return response()->json('No se pudo eliminar la categoria de esa pregunta', 500);
            }
        } else {
            return response()->json('No se encontro la pregunta seleccionada.', 500);
        }
    }
    public function get_preguntas()
    {
        $preg = Pregunta::select(['id', 'pregunta_modelo', 'categoria_id'])->whereIn('categoria_id', Categoria::select('id')->where('bot_id', Session::get('bot_id')))->get();
        $collection = collect([]);
        foreach ($preg as $p)
        {
            $es_menu = 0;
            $cat = Categoria::find($p->categoria_id);
            $desc = $cat->descripcion;
            if (substr($desc, -4) !== 'menu')
            {
                $collection->push(['id' => $p->id, 'pregunta_modelo' => $p->pregunta_modelo, 'es_menu' => $es_menu]);
            } 
        }
        return response()->json($collection);
    }
    public function get_menus()
    {
        $preg = Pregunta::select(['id', 'pregunta_modelo', 'categoria_id'])->whereIn('categoria_id', Categoria::select('id')->where('bot_id', Session::get('bot_id')))->get();
        $collection = collect([]);
        foreach ($preg as $p) {
            $es_menu = 0;
            $cat = Categoria::find($p->categoria_id);
            $desc = $cat->descripcion;
            if (substr($desc, -4) == 'menu') {
                $es_menu = 1;
                $collection->push(['id' => $p->id, 'pregunta_modelo' => $p->pregunta_modelo, 'es_menu' => $es_menu]);
            }
        }
        return response()->json($collection);
    }
}