@extends('voyager::master')
@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<div class="container">
    <div class="navbar-template text-center">
        <h1>Administración de Diagramas</h1>
        <h3 class="lead text-success">Modificar, crear, quitar y entrenar respuestas del BOT</h3>
    </div>
</div>
<div class="collapse-group">
    <div class="controls">
        <button class="btn btn-primary open-button" type="button">
            Abrir Todas
        </button>
        <button class="btn btn-primary close-button" type="button">
            Cerrar Todas
        </button>
        <button class="btn btn-success" type="button" onclick="javascript:newMainMenu()">
            Nueva Opcion del Menú Principal
        </button>
    </div>
    @foreach ($menu as $m)
        @if ($m->hasChilds())
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading_{{$m->id}}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapse_{{$m->id}}" aria-expanded="true" aria-controls="collapse_{{$m->id}}" class="trigger collapsed">
                        {{ $m->letra.' - '.$m->descripcion }} || 
                        @if ($m->hasChilds())
                        <i class="em em-abc"></i> -
                        @endif
                        <a href="#" onclick="javascript:opciones_menu({{$m->respuesta_id}}, {{$m->pregunta_id}})" target="_blank" class="dropdown-toggle"
                            data-toggle="dropdown"><i class="em em-arrow_right"></i> Configuración <i class="em em-arrow_left"></i></a>
                    </a>
                </h4>
            </div>
            <div id="collapse_{{$m->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$m->id}}">
                <div class="panel-body">
                    <p style="color:cornflowerblue"><i class="em em-robot_face"></i><i> Fuí entrenada con ésta opción {{$m->getTrainedTimes()}} veces. </i><a href="https://chatbot.proyectoaz.com/public/admin/preguntas-usuarios?key=pregunta_id&filter=equals&s={{$m->respuesta_id}}" target="_blank"><i class="em em-eyes"></i></a></p>
                    @foreach ($m->getChilds() as $child)
                    <div class="panel panel-default child_con_hijos">
                        <div class="panel-heading" role="tab" id="heading_{{$child->id}}">
                            <h4 class="panel-title">
                                 <a role="button" data-toggle="collapse" href="#collapse_{{$child->id}}" aria-expanded="true"
                                    aria-controls="collapse_{{$child->id}}" class="trigger collapsed">
                                    {{ $child->letra.' - '.$child->descripcion }} || 
                                    @if ($child->hasChilds())
                                    <i class="em em-abc"></i> - 
                                    @endif
                                    <a href="#"
                                        onclick="javascript:opciones_menu({{$child->respuesta_id}}, {{$child->pregunta_id}})" target="_blank" class="dropdown-toggle"
                                        data-toggle="dropdown"><i class="em em-arrow_right"></i> Configuración <i class="em em-arrow_left"></i></a>
                            </h4>
                        </div>
                        <div id="collapse_{{$child->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$child->id}}">
                            <div class="panel-body">
                                <p style="color:cornflowerblue"><i class="em em-robot_face"></i><i> Fuí entrenada con ésta opción {{$child->getTrainedTimes()}} veces. </i><a href="https://chatbot.proyectoaz.com/public/admin/preguntas-usuarios?key=pregunta_id&filter=equals&s={{$child->respuesta_id}}" target="_blank"><i class="em em-eyes"></i></a></p>
                            @if ($child->hasChilds())
                            @foreach ($child->getChilds() as $sub_childs)
                                <div class="panel panel-default sub_childs_con_hijos">
                                    <div class="panel-heading" role="tab" id="heading_{{$sub_childs->id}}">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" href="#collapse_{{$sub_childs->id}}" aria-expanded="true"
                                                aria-controls="collapse_{{$sub_childs->id}}" class="trigger collapsed">
                                                {{ $sub_childs->letra.' - '.$sub_childs->descripcion }} ||
                                                @if ($sub_childs->hasChilds())
                                                <i class="em em-abc"></i> -
                                                @endif
                                                <a href="#" onclick="javascript:opciones_menu({{$sub_childs->respuesta_id}}, {{$sub_childs->pregunta_id}})" target="_blank" class="dropdown-toggle"
                                                    data-toggle="dropdown"><i class="em em-arrow_right"></i> Configuración <i class="em em-arrow_left"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse_{{$sub_childs->id}}" class="panel-collapse collapse" role="tabpanel"
                                        aria-labelledby="heading_{{$sub_childs->id}}">
                                        <div class="panel-body">
                                            <p style="color:cornflowerblue"><i class="em em-robot_face"></i><i> Fuí entrenada con ésta opción {{$sub_childs->getTrainedTimes()}} veces. </i><a href="https://chatbot.proyectoaz.com/public/admin/preguntas-usuarios?key=pregunta_id&filter=equals&s={{$sub_childs->respuesta_id}}" target="_blank"><i class="em em-eyes"></i></a></p>
                                            @if ($sub_childs->hasChilds())
                                            @foreach ($sub_childs->getChilds() as $sub_sub_childs)
                                                <div class="panel panel-default sub_sub_childs_con_hijos">
                                                    <div class="panel-heading" role="tab" id="heading_{{$sub_sub_childs->id}}">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" href="#collapse_{{$sub_sub_childs->id}}" aria-expanded="true"
                                                                aria-controls="collapse_{{$sub_sub_childs->id}}" class="trigger collapsed">
                                                                {{ $sub_sub_childs->letra.' - '.$sub_sub_childs->descripcion }} ||
                                                                @if ($sub_sub_childs->hasChilds())
                                                                <i class="em em-abc"></i> -
                                                                @endif
                                                                <a href="#" onclick="javascript:opciones_menu({{$sub_sub_childs->respuesta_id}}, {{$sub_sub_childs->pregunta_id}})" target="_blank"
                                                                    class="dropdown-toggle" data-toggle="dropdown"><i class="em em-arrow_right"></i> Configuración <i
                                                                        class="em em-arrow_left"></i></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_{{$sub_sub_childs->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$sub_sub_childs->id}}">
                                                        <div class="panel-body">
                                                            <p style="color:cornflowerblue"><i class="em em-robot_face"></i><i> Fuí entrenada con ésta opción {{$sub_sub_childs->getTrainedTimes()}} veces. </i><a href="https://chatbot.proyectoaz.com/public/admin/preguntas-usuarios?key=pregunta_id&filter=equals&s={{$sub_sub_childs->respuesta_id}}" target="_blank"><i class="em em-eyes"></i></a></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @else
        <!-- SI EL MENU PRINCIPAL NO TIENE SUBMENUES -->
        <div class="panel panel-default m_no_hijos">
            <div class="panel-heading" role="tab" id="heading_{{$m->id}}">
                <h4 class="panel-title">
                   <a role="button" data-toggle="collapse" href="#collapse_{{$m->id}}" aria-expanded="true"
                    aria-controls="collapse_{{$m->id}}" class="trigger collapsed">
                    {{ $m->letra.' - '.$m->descripcion }} ||
                    <a href="#" onclick="javascript:opciones_menu({{$m->respuesta_id}}, {{$m->pregunta_id}})" target="_blank" class="dropdown-toggle"
                        data-toggle="dropdown"><i class="em em-arrow_right"></i> Configuración <i class="em em-arrow_left"></i></a>
                </a>
                </h4>
            </div>
            <div id="collapse_{{$m->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$m->id}}">
                <div class="panel-body">
                    <p style="color:cornflowerblue"><i class="em em-robot_face"></i><i> Fuí entrenada con ésta opción {{$m->getTrainedTimes()}}
                        veces. </i><a href="https://chatbot.proyectoaz.com/public/admin/preguntas-usuarios?key=pregunta_id&filter=equals&s={{$m->respuesta_id}}" target="_blank"><i class="em em-eyes"></i></a></p>
                </div>
            </div>
        </div>
        @endif
    @endforeach
</div>
@stop
@section('css')
@include('vendor.voyager.css')
<style>
    .panel-title {
    display: block;
    padding: 10px 10px 10px 10px;
    font-size: 12px;
    text-align: left;
    }
    .controls {
    margin-bottom: 10px;
    }
    
    .collapse-group {
    padding: 10px;
    border: 1px solid darkgrey;
    margin-bottom: 10px;
    }
    
    .panel-title .trigger:before {
    content: '\e082';
    font-family: 'Glyphicons Halflings';
    vertical-align: text-bottom;
    }
    
    .panel-title .trigger.collapsed:before {
    content: '\e081';
    }
    .dropdown-submenu {
    position: relative;
    }
    
    .dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
    }
    
    .dropdown-submenu:hover>.dropdown-menu {
    display: block;
    }
    
    .dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
    }
    
    .dropdown-submenu:hover>a:after {
    border-left-color: #fff;
    }

    .dropdown-submenu.pull-left {
    float: none;
    }
    
    .dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
    }
    .modal-dialog {
        width: 80%;
        /* New width for default modal */
    }

    form {
        margin: 5px;
    }

    .ui-datepicker {
        z-index: 1151 !important;
    }

    .ui-datepicker-month {
        color: #666
    }

    .ui-datepicker-year {
        color: #666
    }

    .clsDatePicker {
        z-index: 100000;
    }

    body.receipt .sheet {
        width: 58mm;
        height: 100mm
    }

    /* change height as you like */
    @media print {
        body.receipt {
            width: 58mm
        }
    }

    /* this line is needed for fixing Chrome's bug */
</style>
@stop

@section('javascript')

<script>
    $(".open-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('show');
    });
    
    $(".close-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('hide');
    });
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function new_option(padre_id)
{
    Swal.fire({
        title: '¿Crear nueva Respuesta?',
        text: "Por favor verificalo, crear duplicados de respuestas puede perjudicar gravemente el sistema.",
        icon: 'warning',
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'No, ya esta creada.',
        confirmButtonText: 'Si, crear nueva respuesta.'
        }).then((result) => {
        if (result.value) {
            nuevo_hijo(padre_id);
        } else {
            select_hijo(padre_id);
        }
        })
}
function nuevo_hijo(padre_id)
{
    Swal.mixin({
    input: 'textarea',
    confirmButtonText: 'Siguiente &rarr;',
    showCancelButton: true,
    showCloseButton: true,
    progressSteps: ['1', '2', '3', '4']
    }).queue([
    {
        title: 'Categoria',
        text: 'Ingrese un nombre corto y descriptivo'
    },
    {
        title: 'Pregunta Modelo',
        html: '<p>Escriba la pregunta "Modelo" que se mostrará antes de mostrarle la respuesta al usuario.</p><p>Sirve para darle una referencia al usuario de <b style="color:red">que</b> se le está respondiendo exactamente.</p>'
    },
    {
        title: 'Respuesta',
        text: 'Escriba la respuesta, puede usar codigo HTML e iconos de "emoji-css.afeld.me" para embellecerla.'
    },
    {
        title: 'Descripcion',
        text: 'Sera la descripcion con la que se visualizara en el menu del ChatBOT.'
    },
    ]).then((result) => {
    if (result.value[0] && result.value[1] && result.value[2] && result.value[3]) {
        data = {};
        name = 'padre_id';
        data[name] = padre_id;
        name = 'categoria';
        data[name] = result.value[0];
        name = 'pregunta_modelo';
        data[name] = result.value[1];
        name = 'respuesta';
        data[name] = result.value[2];
        name = 'descripcion';
        data[name] = result.value[3];
        $.ajax({
          type: 'POST',
          url: 'add_nuevo_hijo',
          data: data,
          success: function(response) {
              toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
			  setTimeout(function(){ location.reload(); }, 3000);
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
    } else {
        toastr.error('Deben completarse correctamente todos los campos...', "ERROR", {"closeButton": false, "timeOut": "3000"});
    }
    })
}
function select_hijo(padre_id)
{
    var options = {};
  var inputOptionsPromise = new Promise(function (resolve) {
      $.getJSON("get_preguntas", function(data) {
              $.map(data, function(o) {
                  if (o.es_menu == 1) {
                    options[o.id] =  'MENU - '+o.pregunta_modelo;
                  } else {
                    options[o.id] = o.pregunta_modelo;
                  }
                      
                  });
              resolve(options);
              
      });
  });
  const swalWithBootstrapButtons = Swal.mixin({
  confirmButtonText: 'Siguiente &rarr;',
  showCancelButton: true,
  progressSteps: ['1', '2'],
  onBeforeOpen: () => {
    timerInterval = setInterval(() => {
      $('.swal2-input').focus();$('.swal2-select').focus();
    }, 0); 
  },
}).queue([
    {
        input: 'select',
        title: 'Seleccione Respuesta',
        text: 'A continuación se muestra la "Pregunta Modelo" de cada una.',
        inputOptions: inputOptionsPromise
    }, 
    {
        input: 'text',
        title: 'Descripcion',
        text: 'Sera la descripcion con la que se visualizara en el menu del ChatBOT.'
    },
]).then((result) => {
  if (result.value) {
      data = {};
      name = 'pregunta';
      data[name] = result.value[0];
      name = 'descripcion';
      data[name] = result.value[1];
      name = 'padre_id';
      data[name] = padre_id;
      $.ajax({
          type: 'POST',
          url: 'add_hijo_existente',
          data: data,
          success: function(response) {
            toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
            setTimeout(function(){ location.reload(); }, 3000);
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
  }else if (result.dismiss === Swal.DismissReason.cancel) {
      
    }
  });
}
function newMainMenu()
{
 Swal.fire({
    title: '¿Crear nuevo Sub-Menú o una Respuesta?',
    text: "Por favor indícame que quieres hacer.",
    icon: 'warning',
    showCancelButton: true,
    showCloseButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#82cc66',
    cancelButtonText: 'Crear nuevo Sub-Menú dentro del Menú Principal',
    confirmButtonText: 'Crear nueva Respuesta Simple dentro del Menú principal.'
    }).then((result) => {
    if (result.value) {
        nueva_opcion_menu_ppal();
    } else {
        nuevo_submenu_en_menu_ppal();
    }
    })
}
function nueva_opcion_menu_ppal()
{
    Swal.mixin({
    input: 'textarea',
    confirmButtonText: 'Siguiente &rarr;',
    showCancelButton: true,
    showCloseButton: true,
    progressSteps: ['1', '2', '3', '4']
    }).queue([
    {
        title: 'Categoria',
        text: 'Ingrese un nombre corto y descriptivo'
    },
    {
        title: 'Pregunta Modelo',
        html: '<p>Escriba la pregunta "Modelo" que se mostrará antes de mostrarle la respuesta al usuario.</p><p>Sirve para darle una referencia al usuario de <b style="color:red">que</b> se le está respondiendo exactamente.</p>'
    },
    {
        title: 'Respuesta',
        text: 'Escriba la respuesta, puede usar codigo HTML e iconos de "emoji-css.afeld.me" para embellecerla.'
    },
    {
        title: 'Descripcion',
        text: 'Sera la descripcion con la que se visualizara en el menu del ChatBOT.'
    },
    ]).then((result) => {
    if (result.value[0] && result.value[1] && result.value[2] && result.value[3]) {
        data = {};
        name = 'categoria';
        data[name] = result.value[0];
        name = 'pregunta_modelo';
        data[name] = result.value[1];
        name = 'respuesta';
        data[name] = result.value[2];
        name = 'descripcion';
        data[name] = result.value[3];
        $.ajax({
          type: 'POST',
          url: 'add_nueva_opcion_menu_ppal',
          data: data,
          success: function(response) {
              toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
			  setTimeout(function(){ location.reload(); }, 3000);
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
    } else {
        toastr.error('Deben completarse correctamente todos los campos...', "ERROR", {"closeButton": false, "timeOut": "3000"});
    }
    })
}

function nuevo_submenu_en_menu_ppal()
{
    Swal.mixin({
    input: 'textarea',
    confirmButtonText: 'Confirmar',
    showCancelButton: true,
    showCloseButton: true,
    progressSteps: ['1']
    }).queue([
    {
        title: '¿Título del nuevo Sub-Menú?',
        text: 'Debe ser simple e identificatorio.'
    },
    ]).then((result) => {
    if (result.value[0]) {
        data = {};
        name = 'categoria';
        data[name] = result.value[0];
        $.ajax({
          type: 'POST',
          url: 'add_nuevo_menu_al_menu_ppal',
          data: data,
          success: function(response) {
              toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
			  setTimeout(function(){ location.reload(); }, 3000);
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
    } else {
        toastr.error('Deben completarse correctamente todos los campos...', "ERROR", {"closeButton": false, "timeOut": "3000"});
    }
    })
}
function opciones_menu(pregunta_id, hijo_id)
{
    Swal.fire({
        title: '<strong>¿Que deseas hacer?</strong>',
        icon: 'info',
        html:
        '<a href="#" onclick="javascript:train('+pregunta_id+')"><button type="button" class="btn btn-success"> <i class="em em-robot_face"></i> Entrenar</button></a><br />' +
        '<a href="#" onclick="javascript:new_option('+pregunta_id+')"><button type="button" class="btn btn-success" ><i class="em em-new"></i> Agregar Respuesta simple</button></a><br />' +
        '<a href="#" onclick="javascript:crear_submenu_de_menu('+pregunta_id+')"><button type="button" class="btn btn-success" ><i class="em em-abc"></i> Agregar Sub-Menú</button></a><br />' +
         '<a href="https://chatbot.proyectoaz.com/public/admin/preguntas/'+pregunta_id+'/edit" target="_blank"><button type="button" class="btn btn-primary" ><i class="em em-arrows_counterclockwise"></i> Modificar Respuesta</button></a><br/>' +
        '<a href="#" onclick="javascript:quitar_de_menu('+pregunta_id+', '+hijo_id+')"><button type="button" class="btn btn-warning"><i class="em em-exclamation"></i> Quitar <i class="em em-exclamation"></i></button></a><br/>' +
        '<a href="#" onclick="javascript:eliminar('+pregunta_id+')"><button type="button" class="btn btn-danger"><i class="em em-warning"></i> Eliminar <i class="em em-warning"></i></button></a><br/>',
        showCloseButton: true,
        showConfirmButton: false,
        showCancelButton: false,
        focusConfirm: false,
    })
}
function eliminar(pregunta_id)
{
    Swal.fire({
        title: '¿Seguro?',
        html: '<i class="em em-warning"></i> ATENCION!. Se borrarán todas las preguntas entrenadas asociadas a esta respuesta.<br/><br/><i class="em em-warning"></i> NO realices esta acción a menos que estés seguro de lo que estás haciendo.',
        icon: 'warning',
        showCancelButton: true,
        showCloseButton: true,
        focusCancel: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Confirmar',
        }).then((result) => {
        if (result.value) {
           $.ajax({
            type: 'GET',
            url: 'eliminar/'+pregunta_id,
            success: function(response) {
                toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
                setTimeout(function(){ location.reload(); }, 3000);
            },
            error: function(response) {
                toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
            }
            });
        }
    })
}
function crear_submenu_de_menu(pregunta_id)
{
    Swal.fire({
    title: '¿Crear nuevo Sub-Menú o elegir pre-existente?',
    text: "Por favor indícame que quieres hacer.",
    icon: 'warning',
    showCancelButton: true,
    showCloseButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#82cc66',
    cancelButtonText: 'Seleccionar Sub-Menú Preexistente.',
    confirmButtonText: 'Crear Nuevo Sub-Menú',
    }).then((result) => {
    if (result.value) {
        nuevo_sub_menu(pregunta_id);
    } else {
        add_submenu_preexistente(pregunta_id);
    }
    })
}
function nuevo_sub_menu(pregunta_id) 
{
    Swal.mixin({
    input: 'textarea',
    confirmButtonText: 'Siguiente &rarr;',
    showCancelButton: true,
    showCloseButton: true,
    progressSteps: ['1']
    }).queue([
    {
        title: '¿Título del nuevo Sub-Menú?',
        text: 'Debe ser simple e identificatorio.'
    },
    ]).then((result) => {
    if (result.value[0]) {
        data = {};
        name = 'categoria';
        data[name] = result.value[0];
        name = 'padre_id';
        data[name] = pregunta_id;
        $.ajax({
          type: 'POST',
          url: 'add_nuevo_menu_a_menu',
          data: data,
          success: function(response) {
              toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
			  setTimeout(function(){ location.reload(); }, 3000);
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
    } else {
        toastr.error('Deben completarse correctamente todos los campos...', "ERROR", {"closeButton": false, "timeOut": "3000"});
    }
    })
}
function add_submenu_preexistente(pregunta_id)
{
  var options = {};
  var inputOptionsPromise = new Promise(function (resolve) {
      $.getJSON("get_menus", function(data) {
              $.map(data, function(o) {
                  if (o.es_menu == 1) {
                    options[o.id] =  'MENU - '+o.pregunta_modelo;
                  } 
                  });
              resolve(options);
      });
  });
  const swalWithBootstrapButtons = Swal.mixin({
  confirmButtonText: 'Listo! &rarr;',
  showCancelButton: true,
  progressSteps: ['1', '2'],
  onBeforeOpen: () => {
    timerInterval = setInterval(() => {
      $('.swal2-input').focus();$('.swal2-select').focus();
    }, 0); 
  },
}).queue([
    {
        input: 'select',
        title: 'Seleccione Respuesta',
        text: 'A continuación se muestra la "Pregunta Modelo" de cada una.',
        inputOptions: inputOptionsPromise
    }, 
    {
        input: 'text',
        title: 'Descripcion',
        text: 'Sera la descripcion con la que se visualizara en el menu del ChatBOT.'
    },
]).then((result) => {
  if (result.value) {
      data = {};
      name = 'pregunta';
      data[name] = result.value[0];
      name = 'descripcion';
      data[name] = result.value[1];
      name = 'padre_id';
      data[name] = pregunta_id;
      $.ajax({
          type: 'POST',
          url: 'add_hijo_existente',
          data: data,
          success: function(response) {
            toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
            setTimeout(function(){ location.reload(); }, 3000);
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
  }else if (result.dismiss === Swal.DismissReason.cancel) {
      
    }
  });
}
function quitar_de_menu(respuesta_id, pregunta_id)
{
    Swal.fire({
        title: '¿Confirmar?',
        text: "Se borrarán también los sub-menúes que ésta opción pudiera tener.",
        icon: 'warning',
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#82cc66',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Confirmar',
        }).then((result) => {
        if (result.value) {
            data = {};
            name = 'respuesta_id';
            data[name] = respuesta_id;
            name = 'pregunta_id';
            data[name] = pregunta_id;
            $.ajax({
            type: 'POST',
            url: 'quitar_de_menu',
            data: data,
            success: function(response) {
                toastr.success(response, "EXITO!", {"closeButton": false, "timeOut": "3000"});
                setTimeout(function(){ location.reload(); }, 3000);
            },
            error: function(response) {
                toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
            }
            });
        }  
    })
}
function train(pregunta_id)
{
    Swal.fire({
    title: '¡Manos a la obra!',
    input: 'text',
    inputAttributes: {
        autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText:
    '¡<i class="em em-robot_face"></i> Entrenar!',
    showLoaderOnConfirm: true,
    preConfirm: (pregunta) => {
        return fetch('entrenar/'+pregunta_id+'/'+pregunta)
        .then(response => {
            if (!response.ok) {
            throw new Error(response.statusText)
            }
            return response.json()
        })
        .catch(error => {
            Swal.showValidationMessage(
            'Error: No se pudo entrenar esa pregunta.'
            )
        })
    },
    allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
    if (result.value) {
       train(pregunta_id);
    }
    })
}
</script>
@stop