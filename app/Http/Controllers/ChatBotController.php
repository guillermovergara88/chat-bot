<?php

namespace App\Http\Controllers;

use App\Bot;
use App\User;
use App\Pregunta;
use App\PreguntasUsuario;
use App\Categoria;
use App\Diagrama;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Niiknow\Bayes;
use Session;

class ChatBotController extends Controller
{
    public function index()
    {
        if (session()->has('bot_id')) {
            $bot = Bot::where('id', session()->get('bot_id'))->first();
            if ($bot) {
                session()->put('bot_descripcion', $bot->descripcion);
            }
        } else {
            session()->put('bot_id', 1);
            session()->put('bot_descripcion', 'PODER JUDICIAL');
            $bot = Bot::where('id', session()->get('bot_id'))->first();
        }
        
        // $user = User::find(10);
        // Auth::login($user);
        $bienvenida = Pregunta::whereIn('categoria_id', Categoria::select('id')->where('descripcion', 'bienvenida')->where('bot_id', Session::get('bot_id'))->get())->first();
        $image = $bienvenida->picture;
        $hola = $bienvenida->descripcion;
        $title = '<i>ChatBot</i><br/>Corte Suprema de Justicia de Tucumán';
        return view('bot.index', compact('title', 'hola', 'image'));
    }
    public function banear($pregunta_usuario_id)
    {
        $find = PreguntasUsuario::find($pregunta_usuario_id);
        $cat = Categoria::where('descripcion', 'no_tengo_esa_informacion')->where('bot_id', Session::get('bot_id'))->first();
        if ($cat)
        {
            $preg = Pregunta::where('categoria_id', $cat->id)->first();
            $find->pregunta_id = $preg->id;
            $find->save();
            return response()->json('La pregunta fué marcada como Spam.');
        } else {
            return response()->json('No se encontro la categoria de SPAM en el BOT.');
        }
        
    }
    public function disconforme($pregunta_usuario_id)
    {
        $find = PreguntasUsuario::find($pregunta_usuario_id);
        $find->disconforme = 'DISCONFORME';
        $find->save();
        return response()->json('La repsuesta fué marcada en disconformidad.');
    }
    public function set_bot_id(Request $request)
    {
        $id = $request['bot_id'];
        session()->forget('bot_id');
        session()->forget('bot_descripcion');
            $find = Bot::find($id);
            if ($find) {
                session()->put('bot_id', $id);
                session()->put('bot_descripcion', $find->descripcion);
                $resp = $find->descripcion;
                return response()->json($resp, 200);

            } else {
                return response()->json('ChatBOT no encontrado.', 500);
            }
    }
    public function bot_list()
    {
        $bots = Bot::select('id', 'descripcion')->get();
        return response()->json($bots, 200);
    }
    public function send_question(Request $request)
    {
        $collection = collect();
        if (session()->has('diagramas')) {
            $collection = collect(session()->get('diagramas'));
        }
        
        $req = $request->all();
        $pregunta = preg_replace('/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ_ -]/s', '', $req['pregunta']);
        // array con stopwords.
        $stopwords = PreguntasUsuario::stop_words();
        // escape the stopword array and implode with pipe
        $s = '~^\W*('.implode("|", array_map("preg_quote", $stopwords)).')\W+\b|\b\W+(?1)\W*$~i';
        $options['tokenizer'] = function($text) use ($s) {
            // pasamos todo a minusculas.
            $text = mb_strtolower($text);
            // removemos las StopWords.
            $text = preg_replace($s, '', $text);
            // separamos las palabras
            preg_match_all('/[[:alpha:]]+/u', $text, $matches);
            // guardamos las coincidencias.
            return $matches[0];
        };
        $classifier = new Bayes($options);
        // Si la persona está respondiendo con una palabra de más de 1 letra, es porque está redactando y debemos entrenar todas las preguntas
        if (strlen($pregunta) > 1) {
            // Busca todas las preguntas entrenadas.
            $menu = mb_strtolower($pregunta);
            if ($menu == 'menu' or $menu == 'menú') {
                $train_data = PreguntasUsuario::whereIn('pregunta_id', Pregunta::select('id')->whereIn('categoria_id', Categoria::select('id')->where('descripcion', 'menu')->get())->get())->get(); 
            } else {
                $train_data = PreguntasUsuario::whereIn('pregunta_id', Pregunta::select('id')->whereIn('categoria_id', Categoria::select('id')))->where('disconforme', null)->get(); 
            }
            
            //Entrenamos en base a todas las preguntas.
            foreach($train_data as $t) {
                
                $classifier->learn($t->pregunta, $t->pregunta_id);
            }
            //Bayes nos categoriza la pregunta en base a su entrenamiento.
            $clas = $classifier->categorize($pregunta);
            $find = Pregunta::find($clas);
            //Buscamos si hay diagramas relacionados a esa pregunta, en caso que no haya, vaciamos la colección de diagramas.
            $diagramas = $find->Diagramas();
            if ($diagramas === false) {
                session()->forget('diagramas');
            }
            $respuesta = $find->descripcion;
            $pregunta_modelo = $find->pregunta_modelo;
                    // Calcula las probabilidades totales.
            $prob = $classifier->probabilidades($pregunta);
            $acierto = round($prob[$clas] * 100, 2);
                    // serializa el clasificador como un string de JSON. -- No se necesita por ahora esto.
            // $stateJson = $classifier->toJson();
                    // carga el clasificador de vuelta en formato JSON.
            // $classifier->fromJson($stateJson);
                    //Verificamos que la pregunta no exista en la base de datos... En caso de que NO exista, la guardamos y la asociamos a la predicción actual (que luego se podrá modificar)
            $exists = DB::table("preguntas_usuarios")->whereRaw('upper(pregunta) = upper("'.$pregunta.'")')->first();
            if (!$exists) {
                $new = new PreguntasUsuario;
                $new->pregunta_id = $find->id;
                $new->user_id = auth()->user()->id;
                $new->pregunta = $pregunta;
                $new->revisada = 0;
                if ($new->save()) {
                    return response()->json(['accuracy' => $acierto, $respuesta, $diagramas, $find, $new->id, 'pregunta_modelo' => $pregunta_modelo], 200);
                }
            } else {
                return response()->json(['accuracy' => $acierto, $respuesta, $diagramas, $find, 'pregunta_modelo' => $pregunta_modelo], 200);
            }
        } else { 
            //Si solo respondio con 1 letra, suponemos que está seleccionando una opción del Diagrama. Por lo tanto...
            if ($collection->isNotEmpty()) {
                foreach ($collection as $diag) { 
                    //corroboramos a cual de las Opciones del Diagrama guardadas pertenece la respuesta.
                    if ($diag['letra'] == strtoupper($pregunta)) {
                        $find = Pregunta::find($diag['respuesta_id']);
                    }
                }
                if (isset($find)) {
                    $respuesta = $find->descripcion;
                    $pregunta_modelo = $find->pregunta_modelo;
                } else { 
                    //En caso de que escriba una opcion (de 1 letra) que no estaba en el listado.
                    //Verificamos si la coleccion de opciones no esté vacía. (Se vacía al hacer otra pregunta).
                    if ($collection->isEmpty()) { 
                        session()->forget('diagramas');
                    } else {
                        $respuesta = 'La respuesta no corresponde a ninguna de las opciones sugeridas. <h2>😓</h2>';
                    }
                }
            } else { 
                //En caso de que ni siquiera se le hayan propuesto opciones para elegir.
                $respuesta = 'Necesito que escribas la pregunta completa para poder ayudarte. <h2>😓</h2>';
            }
            $diagramas = '';
            if (isset($find)) {
                $diagramas = $find->Diagramas();
                return response()->json([$respuesta, $diagramas, $find, 'pregunta_modelo' => $pregunta_modelo], 200);
            } else {
                return response()->json([$respuesta, $diagramas], 200);
            }
        }
    }
    public function clicked_option($diagrama_id, $letra)
    {
        $collection = collect();
        
        $diag = Diagrama::find($diagrama_id);

        $find = Pregunta::find($diag->pregunta_id);
        $diagramas = $find->Diagramas();
        $collection = collect(session()->get('diagramas'));

        if ($collection->isNotEmpty()) {
            foreach ($collection as $diag) {
                //corroboramos a cual de las Opciones del Diagrama guardadas pertenece la respuesta.
                if ($diag['letra'] == strtoupper($letra)) {
                    $find = Pregunta::find($diag->pregunta_id);
                }
            }
            if (isset($find)) {
                $respuesta = $find->descripcion;
                $pregunta_modelo = $find->pregunta_modelo;
            } else {
                //En caso de que escriba una opcion (de 1 letra) que no estaba en el listado.
                //Verificamos si la coleccion de opciones no esté vacía. (Se vacía al hacer otra pregunta).
                if ($collection->isEmpty()) {
                    session()->forget('diagramas');
                } else {
                    $respuesta = 'La respuesta no corresponde a ninguna de las opciones sugeridas. <h2>😓</h2>';
                }
            }
        } else {
            //En caso de que ni siquiera se le hayan propuesto opciones para elegir.
            $respuesta = 'Necesito que escribas la pregunta completa para poder ayudarte. <h2>😓</h2>';
        }
        $diagramas = '';
        if (isset($find)) {
            $diagramas = $find->Diagramas();
            return response()->json([$respuesta, $diagramas, $find, 'pregunta_modelo' => $pregunta_modelo], 200);
        } else {
            return response()->json([$respuesta, $diagramas], 200);
        }
    }
}