<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\BotPregunta;
use Illuminate\Support\Facades\Session;

class Pregunta extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new BotPregunta);
    }
    public function Diagramas()
    {
        $diag = Diagrama::where('pregunta_id', $this->id)->orderBy('letra', 'asc')->get();
        if ($diag->count() > 0)
        {
            session()->put('diagramas', $diag);
            return $diag;
        } else {
            return false;
        }
    }
}
