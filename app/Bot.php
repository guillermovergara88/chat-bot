<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\BotScope;

class Bot extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new BotScope);
    }
}