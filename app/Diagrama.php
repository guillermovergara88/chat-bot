<?php

namespace App;

use App\Scopes\BotDiagrama;
use Illuminate\Database\Eloquent\Model;


class Diagrama extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new BotDiagrama);
    }
    // RESPUESTA ID es el CHILD, PREGUNTA ID es el PADRE.
    public function getChilds()
    {
        $pregunta = Diagrama::where('pregunta_id', $this->respuesta_id)->orderBy('letra', 'asc')->get();
        return $pregunta;
    }
    public function getParentName() 
    {
        $modelo = Pregunta::find($this->pregunta_id);
        return $modelo->pregunta_modelo;
    }
    public function getChildName()
    {
        $modelo = Pregunta::find($this->respuesta_id);
        return $modelo->pregunta_modelo;
    }
    public function HasChilds()
    {
        $check = Diagrama::where('pregunta_id', $this->respuesta_id)->first();
        if ($check)
        { 
            return true;
        } else {
            return false;
        }
    }
    public function getTrainedTimes()
    {
        $count = PreguntasUsuario::where('pregunta_id', $this->respuesta_id)->where('disconforme', null)->get();
        return $count->count();
    }
}
