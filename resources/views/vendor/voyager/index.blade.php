@extends('voyager::master')
@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<div class="row" id="contenido">
	<div class="chat">

		<div class="contact bar">
			<i class="voyager-double-right" style="position:fixed;top:55px; left:-20px;"></i>
			<div class="pic stark"></div>
			<div class="name">
				ChatBot - Asistente Virtual<br/>
				{!! $title !!}
			</div>
			<div class="seen">
				{{ Carbon\Carbon::now()->format('d-m-Y') }}
			</div>
		</div>
		<div class="messages" id="chat">
			{{-- <div class="time">
				</div> --}}
			<div class="message recieved"></i>{!! $hola !!}</i></div>
		</div>
		<div class="input">
			<input placeholder="Escriba su pregunta..." type="text" id="pregunta" autocomplete="off" /><i id="enviar"
				class="voyager-play"></i>
			{{-- <i class="voyager-megaphone"></i> --}}
		</div>
	</div>
</div>

{{-- <button class="open-button" onclick="openForm()">Chat</button>
<div class="chat-popup" id="myForm">
  <div class="form-container">
	<div class="chat">
			
		<div class="contact bar">
			<i class="voyager-double-right" style="position:fixed;top:55px; left:-20px;"></i>
			<div class="pic stark"></div>
			<div class="name">
				{!! $title !!}
			</div>
			<div class="seen">
				{{ Carbon\Carbon::now()->format('d-m-Y') }}
</div>
</div>
<div class="messages" id="chat">
	<div class="time">
	</div>

</div>
<div class="input">
	<input placeholder="Escriba su pregunta..." type="text" id="pregunta" autocomplete="off" /><i id="enviar"
		class="voyager-play"></i><i class="voyager-megaphone"></i>
</div>
</div>
<button type="button" class="btn cancel" onclick="closeForm()">Cerrar</button>
</div>
</div> --}}
@stop
@section('css')
@include('vendor.voyager.css')
@include('vendor.voyager.1')
<style>
	.modal-dialog {
		width: 80%;
		/* New width for default modal */
	}

	form {
		margin: 5px;
	}

	.ui-datepicker {
		z-index: 1151 !important;
	}

	.ui-datepicker-month {
		color: #666
	}

	.ui-datepicker-year {
		color: #666
	}

	.clsDatePicker {
		z-index: 100000;
	}

	body.receipt .sheet {
		width: 58mm;
		height: 100mm
	}

	/* change height as you like */
	@media print {
		body.receipt {
			width: 58mm
		}
	}

	/* this line is needed for fixing Chrome's bug */
</style>
@stop

@section('javascript')

<script>
	$( document ).ready(function() {
$(window).scrollTop(0);
});

function openForm() {
	document.getElementById("myForm").style.display = "block";
}

function closeForm() {
	document.getElementById("myForm").style.display = "none";
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var input = document.getElementById("pregunta");
input.focus();

input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
    event.preventDefault();
	if (input.value.length > 0)
	{
		send(input.value);
	}
  }
});
$('#enviar').click(function(){
	var value = document.getElementById("pregunta").value;
	if (value.length > 0)
	{
		return send(value);
	}
});
function clicked_option(id, letra)
{
	letra = letra;
	id = id;
	$('#chat').append('<div class="message sent">'+letra+'</div>');
	$.ajax({
          type: 'GET',
          url: 'clicked_option/'+id+'/'+letra,
          success: function(response) {
			  data = {};
	name = 'pregunta';
	data[name] = letra;
	$.ajax({
          type: 'POST',
          url: 'send_question',
          data: data,
          success: function(response) {
					$('.typing').hide();
					if (response['accuracy']) {
						// $('#chat').append('<div class="message recieved"><i style="color: gray"> Esta respuesta tiene la mayor probabilidad de todas las opciones, con '+response['accuracy']+'%.</i></div>');
					}
					$('#chat').append('<div class="message recieved">'+response[0]+'</div>');
					
					if (response[1] !== '[]') {
						$.each(response[1], function(k, v) {
							$('#chat').append('<div class="message recieved"><span  style="color:green;"><a href="javascript:clicked_option('+v['id']+', \''+v['letra']+'\')">'+v['letra']+'. -</span> '+v['descripcion']+'</a></div>');
						});
					} else {
						$('#chat').append('<div class="message recieved">'+response[0]+'</div>');
					}
					if (response[2]) {
						if (response[2]['gmaps']) {
							$('#chat').append('<div class="message recieved"><a href="'+response[2]['gmaps']+'" target="_blank"><button type="button" class="btn btn-success">Mapa <i class="voyager-world"></i></button></a></div>');
						}
						if (response[2]['picture']) {
							$('#chat').append('<div class="message recieved"><img width="150px" src="'+window.location.origin+'/public/storage/'+response[2]['picture']+'"></img></div>');
						}
						if (response[3]) {
							$('#chat').append('<div class="message recieved"><i>¿No estás satisfecho con la respuesta?</br> 1) <a href="'+window.location.href+'/preguntas-usuarios/'+response[3]+'/edit" target="_blank"><button type="button" class="btn btn-warning">Corregirla </button></a><br/>2) <a onclick="banear('+response[3]+')"><button type="button" class="btn btn-warning"> Ignorarla </button></a> (El BOT contestará como "no entendi la pregunta")] <br/><br/></i></div>');
						}
					}
					$('html,body').animate({ scrollTop: "+=1280"}, 'slow');
				},
				error: function(response) {
					toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
				}
			});
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
}
function send_void() {
	data = {};
	name = 'pregunta';
	data[name] = 'menu';
	$.ajax({
          type: 'POST',
          url: 'send_question',
          data: data,
          success: function(response) {
			  $('.typing').hide();
			  if (response['accuracy']) {
				// $('#chat').append('<div class="message recieved"><i style="color: gray"> Esta respuesta tiene la mayor probabilidad de todas las opciones, con '+response['accuracy']+'%.</i></div>');
			  }
			  $('#chat').append('<div class="message recieved">'+response[0]+'</div>');
			  
			if (response[1] !== '[]') {
				$.each(response[1], function(k, v) {
					$('#chat').append('<div class="message recieved"><span  style="color:green;"><a href="javascript:clicked_option('+v['id']+', \''+v['letra']+'\')">'+v['letra']+'. -</span> '+v['descripcion']+'</a></div>');
				});
			} else {
				$('#chat').append('<div class="message recieved">'+response[0]+'</div>');
			}
			if (response[2]) {
				if (response[2]['gmaps']) {
					$('#chat').append('<div class="message recieved"><a href="'+response[2]['gmaps']+'" target="_blank"><button type="button" class="btn btn-success">Mapa <i class="voyager-world"></i></button></a></div>');
				}
				if (response[2]['picture']) {
					$('#chat').append('<div class="message recieved"><img width="200px" src="'+window.location.origin+'/public/storage/'+response[2]['picture']+'"></img></div>');
				}
				if (response[3]) {
					$('#chat').append('<div class="message recieved"><i>¿No estás satisfecho con la respuesta?</br> 1) <a href="'+window.location.href+'/preguntas-usuarios/'+response[3]+'/edit" target="_blank"><button type="button" class="btn btn-warning">Corregirla </button></a><br/>2) <a onclick="banear('+response[3]+')"><button type="button" class="btn btn-warning"> Ignorarla </button></a> (El BOT contestará como "no entendi la pregunta")] <br/><br/></i></div>');
				}
			}
			$('html,body').animate({ scrollTop: "+=350"}, 'slow');
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
}
</script>
@if (auth()->user()->role->name == 'guest')
<script>
	// PARA GUESTS
function send(val)
{
	var input = document.getElementById("pregunta");
	var pregunta = input.value;
	$('#chat').append('<div class="message sent">'+val+'</div>');
	const chat = document.getElementById('chat');
	input.value = '';
	data = {};
	name = 'pregunta';
	data[name] = pregunta;
	if (val.length >= 1)
	{
		$('#chat').append('<div class="message recieved typing"><div class="typing typing-1"></div><div class="typing typing-2"></div><div class="typing typing-3"></div></div>');
		$.ajax({
          type: 'POST',
          url: 'send_question',
          data: data,
          success: function(response) {
			  $('.typing').hide();
			  if (response['accuracy']) {
				// $('#chat').append('<div class="message recieved"><i style="color: gray"> Esta respuesta tiene la mayor probabilidad de todas las opciones, con '+response['accuracy']+'%.</i></div>');
			  }
			  if (response['pregunta_modelo']) {
				  $('#chat').append('<div class="message recieved"><i style="color: gray">'+response['pregunta_modelo']+'</i></div>');
			  }
			  $('#chat').append('<div class="message recieved">'+response[0]+'</div>');
			  
			if (response[1] !== '[]') {
				$.each(response[1], function(k, v) {
					$('#chat').append('<div class="message recieved"><span  style="color:green;"><a href="javascript:clicked_option('+v['id']+', \''+v['letra']+'\')">'+v['letra']+'. -</span> '+v['descripcion']+'</a></div>');
				});
			} else {
				$('#chat').append('<div class="message recieved">'+response[0]+'</div>');
			}
			if (response[2]) {
				if (response[2]['gmaps']) {
					$('#chat').append('<div class="message recieved"><a href="'+response[2]['gmaps']+'" target="_blank"><button type="button" class="btn btn-success">Mapa <i class="voyager-world"></i></button></a></div>');
				}
				if (response[2]['picture']) {
					$('#chat').append('<div class="message recieved"><img width="200px" src="'+window.location.origin+'/public/storage/'+response[2]['picture']+'"></img></div>');
				}
				if (response[3]) {
					$('#chat').append('<div class="message recieved"><i>Esa pregunta es nueva!, Si no estás satisfecho/a con la respuesta haz click abajo.</br><a onclick="disconforme('+response[3]+')"><button type="button" class="btn btn-warning">NO estoy satisfecho/a.</button></a></i></div>');
				}
			}
			$('html,body').animate({ scrollTop: "+=350"}, 'slow');
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
	} else {
		$('#chat').append('<div class="message recieved">Necesito más información para contestar tu pregunta. No soy mago &#129497;, soy un <b>BOT</b> 🤖</div>');
	}
}
</script>
@else
<script>
	function send(val)
{
	var input = document.getElementById("pregunta");
	var pregunta = input.value;
	$('#chat').append('<div class="message sent">'+val+'</div>');
	const chat = document.getElementById('chat');
	input.value = '';
	data = {};
	name = 'pregunta';
	data[name] = pregunta;
	if (val.length >= 1)
	{
		$('#chat').append('<div class="message recieved typing"><div class="typing typing-1"></div><div class="typing typing-2"></div><div class="typing typing-3"></div></div>');
		$.ajax({
          type: 'POST',
          url: 'send_question',
          data: data,
          success: function(response) {
			  $('.typing').hide();
			  if (response['accuracy']) {
				// $('#chat').append('<div class="message recieved"><i style="color: gray"> Esta respuesta tiene la mayor probabilidad de todas las opciones, con '+response['accuracy']+'%.</i></div>');
			  }
			  if (response['pregunta_modelo']) {
				  $('#chat').append('<div class="message recieved"><i style="color: gray">'+response['pregunta_modelo']+'</i></div>');
			  }
			  $('#chat').append('<div class="message recieved">'+response[0]+'</div>');
			  
			if (response[1] !== '[]') {
				$.each(response[1], function(k, v) {
					$('#chat').append('<div class="message recieved"><span  style="color:green;"><a href="javascript:clicked_option('+v['id']+', \''+v['letra']+'\')">'+v['letra']+'. -</span> '+v['descripcion']+'</a></div>');
				});
			} else {
				$('#chat').append('<div class="message recieved">'+response[0]+'</div>');
			}
			if (response[2]) {
				if (response[2]['gmaps']) {
					$('#chat').append('<div class="message recieved"><a href="'+response[2]['gmaps']+'" target="_blank"><button type="button" class="btn btn-success">Mapa <i class="voyager-world"></i></button></a></div>');
				}
				if (response[2]['picture']) {
					$('#chat').append('<div class="message recieved"><img width="200px" src="'+window.location.origin+'/public/storage/'+response[2]['picture']+'"></img></div>');
				}
				if (response[3]) {
					$('#chat').append('<div class="message recieved"><i>¿No estás satisfecho con la respuesta?</br> 1) <a href="'+window.location.href+'/preguntas-usuarios/'+response[3]+'/edit" target="_blank"><button type="button" class="btn btn-warning">Corregirla </button></a><br/>2) <a onclick="banear('+response[3]+')"><button type="button" class="btn btn-warning"> Ignorarla </button></a> (El BOT contestará como "no entendi la pregunta")] <br/><br/></i></div>');
				}
			}
			$('html,body').animate({ scrollTop: "+=350"}, 'slow');
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
	} else {
		$('#chat').append('<div class="message recieved">Necesito más información para contestar tu pregunta. No soy mago &#129497;, soy un <b>BOT</b> 🤖</div>');
	}
}
</script>
@endif
<script>
	function corregir(valor)
{
	$.ajax({
          type: 'GET',
          url: 'corregir/'+valor,
          success: function(response) {
			  toastr.success(response, "LISTO!", {"closeButton": false, "timeOut": "3000"});
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
}
function banear(valor)
{
	$.ajax({
          type: 'GET',
          url: 'banear/'+valor,
          success: function(response) {
			toastr.success(response, "BANEADA", {"closeButton": false, "timeOut": "3000"});
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
}
function disconforme(valor)
{
	$.ajax({
          type: 'GET',
          url: 'disconforme/'+valor,
          success: function(response) {
			toastr.success(response, "Listo! Trabajaremos en ello!.", {"closeButton": false, "timeOut": "3000"});
          },
          error: function(response) {
              toastr.error(response.responseJSON, "ERROR", {"closeButton": false, "timeOut": "3000"});
          }
      });
}
</script>
@stop