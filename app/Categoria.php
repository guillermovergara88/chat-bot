<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\BotCategory;

class Categoria extends Model
{
    protected static function booted()
    {
        static::addGlobalScope(new BotCategory);
    }
}
