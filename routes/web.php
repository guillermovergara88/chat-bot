<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::get('admin/abm-diagramas', 'DiagramasController@index');
Route::get('test_bot', 'ChatBotController@index');
Route::post('send_question', 'ChatBotController@send_question');
Route::get('banear/{pregunta_usuario_id}', 'ChatBotController@banear');
Route::get('disconforme/{pregunta_usuario_id}', 'ChatBotController@disconforme');
Route::get('corregir/{pregunta_usuario_id}', 'ChatBotController@corregir');
Route::post('set_bot_id', 'ChatBotController@set_bot_id')->name('set_bot');
Route::get('bot_list', 'ChatBotController@bot_list')->name('bot_list');
Route::get('clicked_option/{diagrama_id}/{letra}', 'ChatBotController@clicked_option');


//Diagramas
Route::post('admin/add_nuevo_hijo', 'DiagramasController@add_hijo');
Route::post('admin/add_hijo_existente', 'DiagramasController@add_hijo_existente');
Route::post('admin/add_nueva_opcion_menu_ppal', 'DiagramasController@add_nueva_opcion_menu_ppal');
Route::post('admin/add_nuevo_menu_al_menu_ppal', 'DiagramasController@add_nuevo_menu_al_menu_ppal');
Route::post('admin/add_nuevo_menu_a_menu', 'DiagramasController@add_nuevo_menu_a_menu');
Route::get('admin/get_preguntas', 'DiagramasController@get_preguntas');
Route::get('admin/get_menus', 'DiagramasController@get_menus');
Route::post('admin/quitar_de_menu', 'DiagramasController@quitar_de_menu');
Route::get('admin/entrenar/{pregunta_id}/{pregunta}', 'DiagramasController@entrenar');
Route::get('admin/eliminar/{pregunta_id}', 'DiagramasController@eliminar');
Route::get('logout', function () {
    Auth::logout();
});